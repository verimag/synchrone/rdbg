- [Installing `rdbg` via opam](#org5c5bd0e)
- [Installing `rdbg` via pre-compiled binaries](#org7cba842)
- [Installing the (git controlled) source](#org07a7945)
- [Useful links](#org952f9a3)
- [Contribute](#org01190ca)

RDBG a programmable debugger that targets reactive programs for which a rdbg-plugin exists. A pre-print of a [scopes 2016 paper is available here](http://www-verimag.imag.fr/DIST-TOOLS/SYNCHRONE/rdbg/rdbg-scopes-pre-print.pdf).


<a id="org5c5bd0e"></a>

# Installing `rdbg` via opam

End users ougth to install `rdbg` via opam.

```sh
$ opam repo add verimag-sync-repo "http://www-verimag.imag.fr/DIST-TOOLS/SYNCHRONE/opam-repository"
$ opam update
$ opam install rdbg
```


<a id="org7cba842"></a>

# Installing `rdbg` via pre-compiled binaries

cf the Install section of <http://www-verimag.imag.fr/DIST-TOOLS/SYNCHRONE/lustre-v6/>


<a id="org07a7945"></a>

# Installing the (git controlled) source

You first need to install dune and gnu-make. To have all the necessary dependencies, it migth help to use opam

```sh
opam install lutils rdbg
opam remove lutils rdbg
```

Then you should install lutils (either via opam or via the git repo) <https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/synchrone/lutils>

Then 'make' should build all the executables, 'make install' should install everything, and 'make test' ougth to exit with a 0 return code.

```sh
opam install lutils ounit
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/synchrone/rdbg/rdbg.git
cd rdbg
make
make install
make test
```


<a id="org952f9a3"></a>

# Useful links

To use the rdbg top-level, the following tip holds

<http://mirror.ocamlcore.org/wiki.cocan.org/tips_for_using_the_ocaml_toplevel.html>


<a id="org01190ca"></a>

# Contribute

To contribute, you need to create an account on <https://gricad-gitlab.univ-grenoble-alpes.fr>

---
