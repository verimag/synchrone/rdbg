#######################################################################################
# Workflow:
# 
# - ((commit|test)* push)
# - push.uv.opam

uv:update_version
update_version:
	git pull
	make clean && make

.PHONY:cia-no-test cia
cia: test cia-no-test
cia-no-test:
	git commit -a -F log && echo "Hint: issue a 'make push'" 

push:
	git push && \
   echo "https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/synchrone/rdbg/pipelines" \
   && echo "if the CI is fine, consider issuing a 'make opam-test'"

merge: merge_work_into_master
merge_work_into_master:
	echo "git checkout master && \
         git merge work && \
         make opam && \
         git push "

amend:
	git commit -F log --amend  -a 

undo:
	git reset HEAD~1 --soft


###############################
# tags

OTAGS=otags
# otags don't manage to parse gnuplotRif.ml
NO_TAGS=gnuplotRif.ml
tags:
	$(OTAGS) -v $(shell ocamlc -where)/*.mli  $(shell ls src/*.ml | grep -v $(NO_TAGS))
tags2:
	$(OTAGS) -v  $(shell ls src/*.ml | grep -v $(NO_TAGS))

###############################
# opam-ing

NAME=rdbg
SRC=lib bin exemples utils scripts doc/slides/main.org README.org LICENCE Makefile Makefile.test Makefile.version *.opam dune-project
$(NAME).$(VERSION).tgz: $(SRC)
	git archive --prefix=$(NAME).$(VERSION)/ -o $@ HEAD $(SRC)

opam-rel : $(NAME).$(VERSION).tgz
	make-opam-pack $(NAME).$(VERSION) $(NAME)

opam-test : $(NAME).$(VERSION).tgz
	make-opam-pack $(NAME).$(VERSION) $(NAME) -test


###############################
# pushing onto the official repo

TGZ=http://www-verimag.imag.fr/DIST-TOOLS/SYNCHRONE/pool/$(NAME).$(VERSION).tgz
opam-pr:
	echo "opam publish $(TGZ) ."

opam-pr-merged:
	git commit -m "fix: a fake commit to protect the tgz used in the main opam repo" --allow-empty   

