;; use org 7.*
(setq load-path (cons (expand-file-name "~jahier/dd/org-mode/share/emacs/site-lisp/") load-path))
(setq load-path (cons (expand-file-name "~jahier/dd/org-mode/lisp/") load-path))

(require 'org-latex)
(require 'org-special-blocks)

;;(setq org-directory "/home/erwan/el_files/org-mode/")
(setq org-export-latex-listings t)
(add-to-list 'org-export-latex-packages-alist '("" "listings"))
(add-to-list 'org-export-latex-packages-alist '("" "color"))
