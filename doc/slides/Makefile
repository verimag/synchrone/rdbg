#----------------------------------------------------------
#
# Slides pdf "animés" avec :
#
# - beamer
# - le splitfig-beamer de Florence
#   qui permet de specifier des animations
#   complexes.
# 
# - N.B. on passe par mk3, un script qui 
#   part d'un .fig et fait tout le boulot
#   (splitfig + fig2dev)
#
# N.B.
# make -r => supprime toutes les regles par défaut
# make -r -d -n => pour voir ce qui ce passe ...
#----------------------------------------------------------
.PHONY : all handout clean

# Supprime les règles de suffixe par défaut
.SUFFIXES :

# Ne rien faire si pas de règle 
.DEFAULT : ;

MAIN=main

FIGS=./figs
OBJDIR =./obj

# 
SRC = $(MAIN).tex


# Les figures 
FIGPDF = \
	$(OBJDIR)/time-travel.tex \
	$(OBJDIR)/v6-compiling-chain.tex \
	$(OBJDIR)/lurette-v3.tex \
	$(OBJDIR)/3d.tex \
	$(OBJDIR)/coroutine.tex \

IMGDIR = \
	$(OBJDIR)/demarche-it.pdf \
	$(OBJDIR)/lurette-v3.pdf \
	$(OBJDIR)/coroutine.pdf \

# La présentation animée
all: $(MAIN).pdf grille.pdf
anim: $(MAIN).pdf
# Handout 2x3
handout: grille.pdf

x:xpdf
xpdf:
	xpdf -fullscreen main.pdf
g:
	xpdf grille.pdf

cp: $(MAIN).pdf
	cp $(MAIN).pdf ../www/rdbg-slides.pdf


# Force la recompil
re :
	make handout

.PHONY:xxx
figures:
	make $(FIGPDF)
$(MAIN).pdf : $(IMGDIR) $(SRC) $(FIGPDF) mybeamer.tex
	mkdir -p $(OBJDIR)
	(cd $(OBJDIR); pdflatex --shell-escape  ../$(MAIN).tex)
	mv $(OBJDIR)/$(MAIN).pdf .

$(OBJDIR)/$(MAIN)-handout.pdf: $(OBJDIR)/$(MAIN)-handout.tex $(SRC)
	mkdir -p $(OBJDIR)
	(cd $(OBJDIR); pdflatex --shell-escape  $(MAIN)-handout.tex)
	cp $(OBJDIR)/$(MAIN)-handout.pdf .

grille.pdf : grille.tex  $(OBJDIR)/$(MAIN)-handout.pdf
	mkdir -p $(OBJDIR)
	(cd $(OBJDIR); pdflatex --shell-escape  ../grille.tex)
	mv $(OBJDIR)/grille.pdf .

$(OBJDIR)/$(MAIN)-handout.tex: $(MAIN).tex
	mkdir -p $(OBJDIR)
	sed -e 's/\\documentclass\[\(.*\)\]{beamer}/\n\
\\documentclass\[\1,handout\]{beamer}\n\
/' $(MAIN).tex > $(OBJDIR)/$(MAIN)-handout.tex

# $(FIGS)/%.fig -> $(OBJDIR)/%.tex + toutes les couches 
$(OBJDIR)/%.tex : $(FIGS)/%.fig
	mkdir -p $(OBJDIR)
	cd $(FIGS); mk3 -b -o ../$(OBJDIR) $*.fig

# $(FIGS)/%.jpg -> $(OBJDIR)/%.jpg
$(OBJDIR)/%.jpg : $(OBJDIR) $(FIGS)/%.jpg
	cp $(FIGS)/$*.jpg $(OBJDIR)/$*.jpg

$(OBJDIR)/%.gif : $(OBJDIR) $(FIGS)/%.gif
	cp $(FIGS)/$*.gif $(OBJDIR)/$*.gif

$(OBJDIR)/%.pdf : $(OBJDIR) $(FIGS)/%.pdf
	cp $(FIGS)/$*.pdf $(OBJDIR)/$*.pdf

clean:
	/bin/rm -rf ./$(OBJDIR) $(MAIN).tex $(MAIN)_.tex *.pdf 

EMACS=emacs --batch \
           --load=emacs-org.el

%.tex: %.org 
	$(EMACS) --visit=$*.org --funcall org-export-as-latex
	cp $*.tex $*_.tex; sed -e "s/\\\usepackage{wasysym}//" $*_.tex > $*.tex

